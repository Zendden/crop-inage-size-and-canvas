<?php

    /**
     * Path to image file as example .png
     */
    $pngPath    = realpath ("D:\JaredsPlace\OSPanel\domains\stackoverflow\shark_PNG18835.png");

    /**
     * Функция для сжатия с возможным обрезанием изображения.
     * 
     * @param string $imgPath   | Путь до изображения
     * @param int $ratio        | Степень уменьшения изображение (по умолчанию в три раза (было 900 -> станет 300), может варьироваться от 1 до 9)
     * @param bool $save        | Если параметр не задан или имеет значение false, то изображение будет выводиться в браузере
     * @param int $quality      | Качество, может варьироватьтся от 0 до 9
     * @param int $imgWidth     | Ширина изображения (нужно учитывать, что они будут уменьшены в $ratio раз, можно выставить $ratio = 1, и задать желаемые размеры конечного изображени)
     * @param int $imgheight    | Высота изображения (нужно учитывать, что они будут уменьшены в $ratio раз, можно выставить $ratio = 1, и задать желаемые размеры конечного изображения)
     * @param int $cropWidth    | Ширина прямоугольника под изображение (не знаю для каких целей вам это надо)
     * @param int $cropHeight   | Высота прямоугольника под изображение (попробуйте, поиграйтесь)
     * @param int $cropPositionX| Смещение по оси Х
     * @param int $cropPositionY| Смещение по оси Y  
     * @return void
     */
    function cropImg (string $imgPath, int $ratio = 3, bool $save = false, int $quality = 0,
                        int $imgWidth  = null, int $imgHeight  = null,
                        int $cropWidth = null, int $cropHeight = null,
                        int $cropPositionX = 1, int $cropPositionY = 0): void {
        
        /**
         * Если параметр $save не задан, то по дефолту изображение будет выводиться в браузере.
         * В ином случае, создаем рандомную строку для имени файла и сохраняем.
         */
        if (!$save) {
            header ('Content-Type: image/jpg');
        } else {
            $currentDate    = new \DateTime ();
            $currentDate    = date ('Y-m-d', $currentDate->getTimestamp ());
            /**
             * Временное имя файла
             */
            $tmpImgName     = "IMG({$currentDate})";
        }

        /**
         * Проверяем, передан ли путь к изображению.
         */
        if (isset ($imgPath)) {
            /**
             * Если не заданы размеры конечного изображения (пример: 350х500),
             * то получаем их сами и сразу режем в соотношении уменьшения $ratio;
             */
            if (is_null ($imgWidth) and is_null ($imgheight)) {
                $imgAttr = getimagesize ($imgPath);

                $imgWidth   = (int) (round ($imgAttr[0]  / $ratio)); 
                $imgHeight  = (int) (round ($imgAttr[1]  / $ratio));
            }

            /**
             * Создаем новый ресурс изображения.
             */
            $tmpImg     = imagecreatetruecolor ($imgWidth, $imgHeight);

            /**
             * Берем ресурс целевого изображения.
             */
            $newPNG     = imagecreatefrompng ($imgPath);

            /**
             * Если переданы значения высоты и ширины прямоугольника, 
             * для указания размеров создаваемого изображения, то применяем их, если нет, берем их из целевого изображения.
             * (Высота и ширина квадрата, в котором будет размещено ваше изображение)
             */
            $cropWidth  = is_null ($cropWidth)  ? (int) (round (imagesx ($newPNG) / $ratio)) : (int) (round ($cropWidth / $ratio));
            $cropHeight = is_null ($cropHeight) ? (int) (round ((imagesy ($newPNG) / $ratio))) : (int) (round ($cropHeight / $ratio));

            /**
             * Определяем размер полотна (ширина или длинна)
             */
            $cropSize   = ['width' => (int) (round ($cropWidth)), 'height' => (int) (round ($cropHeight))];

            /**
             * Меняем размер изображения с помощью копирования на другое, созданное. 
             * $imgWidth  - ширина результирующего изображения
             * $imgHeight - высота результирующего изображения
             */
            imagecopyresized ($tmpImg, $newPNG, 0, 0, 0, 0, $imgWidth, $imgHeight, imagesx ($newPNG), imagesy ($newPNG));

            /**
             * Передаем ресурс изображения $newPNG и устанавливаем смещение, зачастую они 0-вые (квадрат),
             * X - по горизонтали, Y - по вертикали
             */

            $cropedPNG  = imagecrop (
                $tmpImg, [  'x' => (bool) $cropPositionX ? $cropPositionX : 0, 
                            'y' => (bool) $cropPositionY ? $cropPositionY : 0, 
                            'width' => $cropSize['width'], 
                            'height' => $cropSize['height']
                        ]
            );

            /**
             * Imagecrop может вернуть ресурс или же bool (false)
             */
            if (!is_bool ($cropedPNG)){
                /**
                 * Imagepng|imagejpg|imagebmp|etc - выводят изображения в браузер, если задан только один параметр (ресурс изображения),
                 * второй параметр необзятальный (путь с именем файла, пример: './example.png'), то данная функция не отрисует изображение,
                 * а сохранит его под данным названием по указанному пути,
                 * третий параметр - cтепень сжатия: от 0 (нет сжатия) до 9 (макс сжатие).
                 */
                imagepng ($cropedPNG, !isset ($tmpImgName) ? null : $tmpImgName, $quality);
            } else 
                imagedestroy ($cropedPNG);
        }
    }

cropImg ($pngPath, 1, false, 9);